﻿namespace Twitch
{
    partial class TwitchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TwitchForm));
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.textBoxCanalName = new System.Windows.Forms.TextBox();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.checkBoxAlwaysOnTop = new System.Windows.Forms.CheckBox();
            this.buttonPlayFromListBox = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.smazatZeSeznamuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxShowInTaskbar = new System.Windows.Forms.CheckBox();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStripNotification = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.obnovitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxAspectRatio = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStripNotification.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 0);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.ScriptErrorsSuppressed = true;
            this.webBrowser1.ScrollBarsEnabled = false;
            this.webBrowser1.Size = new System.Drawing.Size(534, 347);
            this.webBrowser1.TabIndex = 0;
            // 
            // textBoxCanalName
            // 
            this.textBoxCanalName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxCanalName.Location = new System.Drawing.Point(12, 12);
            this.textBoxCanalName.Name = "textBoxCanalName";
            this.textBoxCanalName.Size = new System.Drawing.Size(510, 30);
            this.textBoxCanalName.TabIndex = 1;
            this.textBoxCanalName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxCanalName.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Enabled = false;
            this.buttonPlay.Location = new System.Drawing.Point(12, 48);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(75, 23);
            this.buttonPlay.TabIndex = 2;
            this.buttonPlay.Text = "Spustit";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.ButtonPlay_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 26;
            this.listBox1.Location = new System.Drawing.Point(12, 88);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(510, 212);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.ListBox1_SelectedIndexChanged);
            // 
            // checkBoxAlwaysOnTop
            // 
            this.checkBoxAlwaysOnTop.AutoSize = true;
            this.checkBoxAlwaysOnTop.Checked = true;
            this.checkBoxAlwaysOnTop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAlwaysOnTop.Location = new System.Drawing.Point(12, 316);
            this.checkBoxAlwaysOnTop.Name = "checkBoxAlwaysOnTop";
            this.checkBoxAlwaysOnTop.Size = new System.Drawing.Size(86, 17);
            this.checkBoxAlwaysOnTop.TabIndex = 4;
            this.checkBoxAlwaysOnTop.Text = "Vždy nahoře";
            this.checkBoxAlwaysOnTop.UseVisualStyleBackColor = true;
            this.checkBoxAlwaysOnTop.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // buttonPlayFromListBox
            // 
            this.buttonPlayFromListBox.Enabled = false;
            this.buttonPlayFromListBox.Location = new System.Drawing.Point(447, 312);
            this.buttonPlayFromListBox.Name = "buttonPlayFromListBox";
            this.buttonPlayFromListBox.Size = new System.Drawing.Size(75, 23);
            this.buttonPlayFromListBox.TabIndex = 5;
            this.buttonPlayFromListBox.Text = "Přehrát";
            this.buttonPlayFromListBox.UseVisualStyleBackColor = true;
            this.buttonPlayFromListBox.Click += new System.EventHandler(this.ButtonPlayFromListBox_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smazatZeSeznamuToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(177, 26);
            // 
            // smazatZeSeznamuToolStripMenuItem
            // 
            this.smazatZeSeznamuToolStripMenuItem.Image = global::Twitch.Properties.Resources.delete;
            this.smazatZeSeznamuToolStripMenuItem.Name = "smazatZeSeznamuToolStripMenuItem";
            this.smazatZeSeznamuToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.smazatZeSeznamuToolStripMenuItem.Text = "Smazat ze seznamu";
            this.smazatZeSeznamuToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // checkBoxShowInTaskbar
            // 
            this.checkBoxShowInTaskbar.AutoSize = true;
            this.checkBoxShowInTaskbar.Location = new System.Drawing.Point(104, 316);
            this.checkBoxShowInTaskbar.Name = "checkBoxShowInTaskbar";
            this.checkBoxShowInTaskbar.Size = new System.Drawing.Size(162, 17);
            this.checkBoxShowInTaskbar.TabIndex = 6;
            this.checkBoxShowInTaskbar.Text = "Zobrazovat ikonu v taskbaru";
            this.checkBoxShowInTaskbar.UseVisualStyleBackColor = true;
            this.checkBoxShowInTaskbar.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStripNotification;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon1_Click);
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.NotifyIcon1_Click);
            // 
            // contextMenuStripNotification
            // 
            this.contextMenuStripNotification.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.obnovitToolStripMenuItem,
            this.toolStripSeparator1,
            this.konecToolStripMenuItem});
            this.contextMenuStripNotification.Name = "contextMenuStripNotification";
            this.contextMenuStripNotification.Size = new System.Drawing.Size(118, 54);
            // 
            // obnovitToolStripMenuItem
            // 
            this.obnovitToolStripMenuItem.Image = global::Twitch.Properties.Resources.restore;
            this.obnovitToolStripMenuItem.Name = "obnovitToolStripMenuItem";
            this.obnovitToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.obnovitToolStripMenuItem.Text = "Obnovit";
            this.obnovitToolStripMenuItem.Visible = false;
            this.obnovitToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(114, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // konecToolStripMenuItem
            // 
            this.konecToolStripMenuItem.Image = global::Twitch.Properties.Resources.exit;
            this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            this.konecToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.konecToolStripMenuItem.Text = "Konec";
            this.konecToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItem_Click);
            // 
            // checkBoxAspectRatio
            // 
            this.checkBoxAspectRatio.AutoSize = true;
            this.checkBoxAspectRatio.Checked = true;
            this.checkBoxAspectRatio.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAspectRatio.Location = new System.Drawing.Point(272, 316);
            this.checkBoxAspectRatio.Name = "checkBoxAspectRatio";
            this.checkBoxAspectRatio.Size = new System.Drawing.Size(127, 17);
            this.checkBoxAspectRatio.TabIndex = 7;
            this.checkBoxAspectRatio.Text = "Udržovat poměr stran";
            this.checkBoxAspectRatio.UseVisualStyleBackColor = true;
            // 
            // TwitchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 347);
            this.Controls.Add(this.checkBoxAspectRatio);
            this.Controls.Add(this.checkBoxShowInTaskbar);
            this.Controls.Add(this.buttonPlayFromListBox);
            this.Controls.Add(this.checkBoxAlwaysOnTop);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonPlay);
            this.Controls.Add(this.textBoxCanalName);
            this.Controls.Add(this.webBrowser1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "TwitchForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Twitch";
            this.LocationChanged += new System.EventHandler(this.TwitchForm_LocationChanged);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.Resize += new System.EventHandler(this.TwitchForm_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStripNotification.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TextBox textBoxCanalName;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.CheckBox checkBoxAlwaysOnTop;
        private System.Windows.Forms.Button buttonPlayFromListBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem smazatZeSeznamuToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBoxShowInTaskbar;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripNotification;
        private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem obnovitToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.CheckBox checkBoxAspectRatio;
    }
}

