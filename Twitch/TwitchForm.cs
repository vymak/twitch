﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Twitch
{
    public partial class TwitchForm : Form
    {
        private const string FILE = "kanaly.txt";
        private const string PLACEHOLDER = "Zadejte prosím název kanálu nebo kompletní URL";
        private const string URL = "http://cs.twitch.tv/%channel%";
        private const int WM_NCLBUTTONDBLCLK = 0x00A3;

        /// <summary>
        /// Výchozí velikost písma
        /// </summary>
        private float _defaultFontSize;

        /// <summary>
        /// Výchozí šířka okna
        /// </summary>
        private int _defaultWidth;

        /// <summary>
        /// Indikuje, zda-li je uživatel v nastavení či nikoliv
        /// </summary>
        private bool isInSetting = true;

        /// <summary>
        /// Indikuje zda-li je eventa již zeregistrována
        /// </summary>
        private bool isRegistered = false;

        /// <summary>
        /// List dostupných kanálů pro přehrávání
        /// </summary>
        private List<string> listOfCanals;

        //double so division keeps decimal points
        const double widthRatio = 16;
        const double heightRatio = 10.9;

        const int WM_SIZING = 0x214;
        const int WMSZ_LEFT = 1;
        const int WMSZ_RIGHT = 2;
        const int WMSZ_TOP = 3;
        const int WMSZ_BOTTOM = 6;

        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }

        public TwitchForm()
        {
            InitializeComponent();

            webBrowser1.DocumentCompleted += WebBrowser1_DocumentCompleted;
            webBrowser1.PreviewKeyDown += WebBrowser1_PreviewKeyDown;

            textBoxCanalName.LostFocus += TextBox1_LostFocus;
            textBoxCanalName.GotFocus += TextBox1_GotFocus;

            webBrowser1.Visible = false;

            TopMostProp = true;

            _defaultWidth = Width;
            _defaultFontSize = listBox1.Font.Size;

            Height = (int)(heightRatio * Width / widthRatio);

            InitialLoadCanals();
        }

        /// <summary>
        /// Property pro nastavování vždy nahoře
        /// </summary>
        private bool TopMostProp
        {
            get
            {
                return checkBoxAlwaysOnTop.Checked;
            }
            set
            {
                checkBoxAlwaysOnTop.Checked = value;

                if (value)
                    TopMost = true;
                else
                    TopMost = false;
            }
        }

        /// <summary>
        /// Přetížení metody pro povolení dvojkliku na hlavičku okna
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_NCLBUTTONDBLCLK)
            {
                if (webBrowser1.Url != null)
                {
                    SetValues(!isInSetting);
                    isInSetting = !isInSetting;
                }
            }
            if (m.Msg == WM_SIZING && checkBoxAspectRatio.Checked)
            {
                RECT rc = (RECT)Marshal.PtrToStructure(m.LParam, typeof(RECT));
                int res = m.WParam.ToInt32();
                if (res == WMSZ_LEFT || res == WMSZ_RIGHT)
                    rc.Bottom = rc.Top + (int)(heightRatio * Width / widthRatio);
                else if (res == WMSZ_TOP || res == WMSZ_BOTTOM)
                    rc.Right = rc.Left + (int)(widthRatio * Height / heightRatio);
                else if (res == WMSZ_RIGHT + WMSZ_BOTTOM)
                    rc.Bottom = rc.Top + (int)(heightRatio * Width / widthRatio);
                else if (res == WMSZ_LEFT + WMSZ_TOP)
                    rc.Left = rc.Right - (int)(widthRatio * Height / heightRatio);
                Marshal.StructureToPtr(rc, m.LParam, true);
            }
            base.WndProc(ref m);
        }

        private void ButtonPlay_Click(object sender, EventArgs e)
        {
            string url = PrepareUrl();
            if (url.Length != 0)
            {
                webBrowser1.Url = new Uri(url + "/popout");
                SetValues();
                Save(url);
            }
        }

        private void ButtonPlayFromListBox_Click(object sender, EventArgs e)
        {
            SetValues();
            webBrowser1.Url = new Uri(listBox1.SelectedItem.ToString() + "/popout");

            Match match = Regex.Match(listBox1.SelectedItem.ToString(), @"http:\/\/.*\.tv\/(.*)?");
            string nameCanal = listBox1.SelectedItem.ToString();
            if (match.Groups[1].Success)
                nameCanal = match.Groups[1].Value;
            Text = "Twitch | " + nameCanal;
        }

        private void Document_MouseDown(object sender, HtmlElementEventArgs e)
        {
            int x = webBrowser1.Size.Width;
            int y = webBrowser1.Size.Height;

            if (e.MousePosition.X > 0.95 * x && e.MousePosition.Y > 0.95 * y)
                TopMostProp = false;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            int width = Width;
            int height = Height;
            Font current = listBox1.Font;

            float koeficient = ((float)width / _defaultWidth);
            listBox1.Font = new Font(current.FontFamily, koeficient * _defaultFontSize);
            listBox1.Size = new Size(width - 40, height - 160);
            textBoxCanalName.Size = new Size(width - 40, 30);
            checkBoxAlwaysOnTop.Location = new Point(12, listBox1.Location.Y + listBox1.Size.Height + 10);
            checkBoxShowInTaskbar.Location = new Point(104, listBox1.Location.Y + listBox1.Size.Height + 10);
            checkBoxAspectRatio.Location = new Point(272, listBox1.Location.Y + listBox1.Size.Height + 10);
            buttonPlayFromListBox.Location = new Point(width - 30 - buttonPlayFromListBox.Size.Width, listBox1.Location.Y + listBox1.Size.Height + 6);
        }

        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (sender == checkBoxAlwaysOnTop)
                TopMostProp = checkBoxAlwaysOnTop.Checked;
            else if (sender == checkBoxShowInTaskbar)
                ShowInTaskbar = checkBoxShowInTaskbar.Checked;
        }

        private void InitialLoadCanals()
        {
            listOfCanals = new List<string>();
            if (File.Exists(FILE))
            {
                using (StreamReader str = new StreamReader(FILE))
                {
                    string line;
                    while ((line = str.ReadLine()) != null)
                    {
                        listOfCanals.Add(line);
                        listBox1.Items.Add(line);
                    }
                }
            }
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                buttonPlayFromListBox.Enabled = true;
                listBox1.ContextMenuStrip = contextMenuStrip1;
            }
        }

        private void NotifyIcon1_Click(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                RestoreFormFromNotificationArea();
        }

        private string PrepareUrl()
        {
            if (Uri.IsWellFormedUriString(textBoxCanalName.Text, UriKind.Absolute) && Regex.IsMatch(textBoxCanalName.Text, @"(http|https):\/\/.*twitch.tv"))
                return (Regex.IsMatch(textBoxCanalName.Text, "/popout"))
                    ? Regex.Replace(textBoxCanalName.Text, "/popout", String.Empty)
                    : textBoxCanalName.Text;

            if (!Regex.IsMatch(textBoxCanalName.Text, @"(http|https|[.\/:])"))
            {
                string url = URL;
                return Regex.Replace(url, "%channel%", textBoxCanalName.Text);
            }
            else
            {
                MessageBox.Show("Neplatný tvar odkazu! Zkuste zadat platný tvar", "Chyba kanálu", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return String.Empty;
            }
        }

        private void RestoreFormFromNotificationArea()
        {
            Show();
            WindowState = FormWindowState.Normal;
            toolStripSeparator1.Visible = false;
            obnovitToolStripMenuItem.Visible = false;

            if (webBrowser1.Url != null)
                webBrowser1.Refresh(WebBrowserRefreshOption.Completely);
        }

        private void Save(string url, bool append = true)
        {
            using (StreamWriter writer = new StreamWriter(FILE, append))
            {
                if (!listOfCanals.Contains(url))
                {
                    listOfCanals.Add(url);
                    writer.WriteLine(url);
                    listBox1.Items.Add(url);
                }
            }
        }

        private void SetValues(bool show = true)
        {
            webBrowser1.Visible = show;
            buttonPlay.Visible = !show;
            buttonPlayFromListBox.Visible = !show;
            textBoxCanalName.Visible = !show;
            listBox1.Visible = !show;
            checkBoxAlwaysOnTop.Visible = !show;
            checkBoxShowInTaskbar.Visible = !show;
            checkBoxAspectRatio.Visible = !show;
        }

        private void TextBox1_GotFocus(object sender, EventArgs e)
        {
            textBoxCanalName.ForeColor = Color.Black;
            if (textBoxCanalName.Text == PLACEHOLDER)
                textBoxCanalName.Text = "";
        }

        private void TextBox1_LostFocus(object sender, EventArgs e)
        {
            if (textBoxCanalName.Text.Length == 0)
            {
                textBoxCanalName.ForeColor = Color.Gray;
                textBoxCanalName.Text = PLACEHOLDER;
                buttonPlay.Enabled = false;
            }
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBoxCanalName.Text.Length > 0)
                buttonPlay.Enabled = true;
            else
                buttonPlay.Enabled = false;
        }

        private void ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender == smazatZeSeznamuToolStripMenuItem)
            {
                if (listBox1.SelectedIndex != -1)
                {
                    listOfCanals.Remove(listBox1.SelectedItem.ToString());
                    listBox1.Items.Clear();
                    using (StreamWriter str = new StreamWriter(FILE, false))
                    {
                        foreach (var item in listOfCanals)
                        {
                            str.WriteLine(item);
                            listBox1.Items.Add(item);
                        }
                    }
                }
                else
                    MessageBox.Show("Než budete moci odstanit položku ze seznamu, musíte ji neprve vybrat", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (sender == konecToolStripMenuItem)
                Close();
            else if (sender == obnovitToolStripMenuItem)
                RestoreFormFromNotificationArea();
        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (webBrowser1.Document != null)
            {
                webBrowser1.Document.Body.Style = "overflow:hidden";

                if (!isRegistered)
                {
                    isRegistered = true;
                    webBrowser1.Document.MouseDown += Document_MouseDown;
                }
            }
        }

        private void WebBrowser1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
                webBrowser1.Refresh(WebBrowserRefreshOption.Completely);
        }

        private void TwitchForm_LocationChanged(object sender, EventArgs e)
        {
            int workingHeight = Screen.PrimaryScreen.WorkingArea.Height;
            int workingWidth = Screen.PrimaryScreen.WorkingArea.Width;

            int offset = 15;
            int currentLocationWidth = workingWidth - (Location.X + Size.Width);
            int currentLocationHeight = workingHeight - (Location.Y + Size.Height);

            if (currentLocationWidth < offset && currentLocationHeight < offset)
                Location = new Point(workingWidth - Size.Width, workingHeight - Size.Height);
            else if (Location.X < offset && currentLocationHeight < offset)
                Location = new Point(0, workingHeight - Size.Height);
            else if (Location.X < offset && Location.Y < offset)
                Location = new Point(0, 0);
            else if (currentLocationWidth < offset && Location.Y < offset)
                Location = new Point(workingWidth - Size.Width, 0);
        }

        private void TwitchForm_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Minimized && !ShowInTaskbar)
            {
                toolStripSeparator1.Visible = true;
                obnovitToolStripMenuItem.Visible = true;
                notifyIcon1.ShowBalloonTip(5000, "Twitch stále běží", "Program byl minimalizován a stream byl pozastaven. Obnovení programu a streamu provedete kliknutím na tuto ikonu.", ToolTipIcon.Info);
                Hide();
                webBrowser1.Stop();
            }
        }

    }
}